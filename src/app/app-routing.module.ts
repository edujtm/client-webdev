import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResourceCreateComponent } from './components/resources/resource-create/resource-create.component';
import { TabCreateComponent } from './components/tabs/tab-create/tab-create.component';
import { WorkspaceCreateComponent } from './components/workspaces/workspace-create/workspace-create.component';
import { WorkspaceReadComponent } from './components/workspaces/workspace-read/workspace-read.component';
import { WorkspaceUpdateComponent } from './components/workspaces/workspace-update/workspace-update.component';
import { WorkspaceNavComponent } from './components/template/workspace-nav/workspace-nav.component';
import { HomeComponent } from './views/home/home.component';
import { LoginComponent } from './views/login/login.component';
import { SignUpComponent } from './views/sign-up/sign-up.component';
import { LoginCardComponent } from './views/login-card/login-card.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: WorkspaceNavComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'workspaces/:workspaceId/resources/create',
        component: WorkspaceCreateComponent
      },
      {
        path: 'workspaces/:workspaceId/resources/:resourceId',
        component: WorkspaceReadComponent
      },
      {
        path: 'workspaces/:workspaceId/resources/:resourceId/edit',
        component: WorkspaceUpdateComponent
      },
      {
        path: 'workspaces/create',
        component: ResourceCreateComponent
      },
      {
        path: 'workspaces/:workspaceId/resources/:resourceId/tabs/create',
        component: TabCreateComponent
      },
    ]
  },
  {
    path: 'auth',
    component: LoginCardComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'sign-up',
        component: SignUpComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
