import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { Tab } from 'src/app/models/Tab';

@Injectable({
  providedIn: 'root'
})
export class TabService {

  url = 'https://localhost:5001';

  constructor(private http: HttpClient, private snackBar: MatSnackBar) { }

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'x', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success']
    });
  }

  getAll(workspaceId: number, resourceId: number): Observable<Tab[]> {
    return this.http.get<Tab[]>(`${this.url}/api/resource/${resourceId}/workspace/${workspaceId}/tabs`);
  }

  post(workspaceId: number, newTab: Tab): Observable<Tab> {
    return this.http.post<Tab>(`${this.url}/api/tab`, { ...newTab, workspaceId });
  }

  delete(workspaceId: number, resourceId: number, id: number):Observable<Tab> {
    return this.http.delete<Tab>(`${this.url}/api/tab/${id}/resource/${resourceId}/workspace/${workspaceId}`);
  }
}
