import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { JwtToken } from '../models/jwt-token.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly BASE_URL = 'https://localhost:5001/api/auth'
  static readonly JWT_TOKEN_INFO = 'token_info'
  static readonly USER_INFO = 'user_info'
  public user = new BehaviorSubject<UserInfo>(null);

  constructor(
    private http: HttpClient
  ) {
    const userInfo : UserInfo = JSON.parse(localStorage.getItem(AuthService.USER_INFO));
    this.user.next(userInfo);
  }

  login(username: string, password: string) : Observable<AuthInfo> {
    return this.http.post<AuthInfo>(
      `${this.BASE_URL}/login`,
      { userName: username, password }
    ).pipe(
      tap((authInfo) => {
        localStorage.setItem(
          AuthService.JWT_TOKEN_INFO, 
          JSON.stringify(authInfo.tokenInfo)
        );
        localStorage.setItem(
          AuthService.USER_INFO,
          JSON.stringify(authInfo.userInfo)
        )
        this.user.next(authInfo.userInfo);
      })
    );
  }

  signUp(data: SignUpData) : Observable<AuthInfo> {
    return this.http.post<AuthInfo>(
      `${this.BASE_URL}/sign-up`,
      { ...data }
    ).pipe(
      tap((authInfo) => {
        localStorage.setItem(
          AuthService.JWT_TOKEN_INFO, 
          JSON.stringify(authInfo.tokenInfo)
        )
        localStorage.setItem(
          AuthService.USER_INFO,
          JSON.stringify(authInfo.userInfo)
        )
        this.user.next(authInfo.userInfo);
      })
    );
  }

  refreshToken() : Observable<JwtToken> {
    return this.http.get<TokenInfo>(
      `${this.BASE_URL}/refresh-token`
    ).pipe(
      tap((tokenInfo: TokenInfo) => {
        localStorage.setItem(
          AuthService.JWT_TOKEN_INFO, 
          JSON.stringify(tokenInfo)
        )
      }),
      map((tokenInfo: TokenInfo) => {
        return JwtToken.from(tokenInfo)
      })
    );
  }

  logout() {
    localStorage.removeItem(AuthService.JWT_TOKEN_INFO);
    localStorage.removeItem(AuthService.USER_INFO);
    this.user.next(null);
  }

}

export interface SignUpData {
  password: string;
  passwordConfirmation: string;
  userName: string;
  email: string;
}

export interface AuthInfo {
  userInfo: UserInfo
  tokenInfo: TokenInfo
}

export interface UserInfo {
  userId: number;
  userName: string;
  email: string;
}

export interface TokenInfo {
  token: string;
  expiration: string;
}
