import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Workspace } from 'src/app/models/Workspace';

@Injectable({
  providedIn: 'root'
})
export class WorkspacesService {

  constructor(private snackBar: MatSnackBar, private http: HttpClient, private route: ActivatedRoute) { }

  url = 'https://localhost:5001';

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'x', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success']
    });
  }

  getAll(): Observable<Workspace[]> {
    return this.http.get<Workspace[]>(`${this.url}/api/user/workspaces`);
  }

  post(workspace: Workspace): Observable<Workspace> {
    return this.http.post<Workspace>(`${this.url}/api/workspace`, workspace);
  }
}
