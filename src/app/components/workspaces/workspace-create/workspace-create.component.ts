import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Resource } from 'src/app/models/Resource';
import { ResourcesService } from '../../resources/resources.service';

@Component({
  selector: 'app-workspace-create',
  templateUrl: './workspace-create.component.html',
  styleUrls: ['./workspace-create.component.css']
})
export class WorkspaceCreateComponent implements OnInit {
  resource: Resource = {
    name: '',
    workspaceId: 0
  };

  workspaceId: number;

  constructor(private route: ActivatedRoute, private router: Router, private resourceService: ResourcesService) { }

  ngOnInit(): void {
    this.workspaceId = this.route.snapshot.params.workspaceId;
    this.resource.name = "";
    this.resource.workspaceId = +this.workspaceId;
  }


  create() {
    this.resourceService.post(this.resource).subscribe(() => {
      this.resourceService.showMessage("Resource criado com sucesso!");
      this.router.navigate(['/workspaces/id/resources/id']);
    });
  }

  cancel() {
    this.router.navigate(['/']);
  } 

}
