
import { TokenInfo } from '../services/auth.service';

export class JwtToken {

  constructor(
    public accessToken : string,
    public expiresAt: Date
  ) {}

  static from(token_info: TokenInfo) : JwtToken {
    const token = token_info.token;
    const expiresAt = new Date(token_info.expiration);
    return new JwtToken(token, expiresAt);
  }

  /**
   * Se essa token já está expirada.
   */
  get isExpired() {
    return this.expiresAt.getTime() - new Date().getTime() < 60;
  }
}
