import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { Resource } from 'src/app/models/Resource';

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {

  url = 'https://localhost:5001'

  constructor(private http: HttpClient, private snackBar: MatSnackBar) { }

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'x', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success']
    });
  }

  getById(workspaceId: number, id: number): Observable<Resource> {
    return this.http.get<Resource>(`${this.url}/api/resource/${id}/workspace/${workspaceId}`);
  }

  getAllByWorkspaceId(workspaceId: number): Observable<Resource[]> {
    return this.http.get<Resource[]>(`${this.url}/api/workspace/${workspaceId}/resources`);
  }

  post(newResource: Resource): Observable<Resource> {
    return this.http.post<Resource>(`${this.url}/api/resource`, newResource);
  }

  put(resourceId: number, workspaceId: number, resourceName: string): Observable<Resource> {
    return this.http.put<Resource>(`${this.url}/api/resource/${resourceId}/workspace/${workspaceId}/name`, { name: resourceName });
  }
}
